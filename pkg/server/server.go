package server

import (
	"archive/tar"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"strings"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/client"
	"github.com/docker/docker/pkg/jsonmessage"
	"golang.org/x/net/context"

	pb "github.com/zekoz/conscription/pkg/conscription"
)

var (
	freecivRepo = flag.String("freeciv_repo", "https://github.com/freeciv/freeciv", "Repo to build freeciv binary from")
)

type Server struct {
	Client *client.Client
}

const Dockerfile = `
FROM phusion/baseimage:latest as build

RUN apt-get update && apt-get install -y libcurl4-openssl-dev libjansson-dev make automake autoconf libbz2-dev liblzma-dev libicu-dev pkg-config zlib1g-dev libsqlite3-dev zip python3 libtool

ARG freeciv_commit

WORKDIR build

RUN curl "%s/archive/$freeciv_commit.tar.gz" \
  -Ls | tar xz --strip-components=1 && ./autogen.sh \
  --enable-debug --disable-fcmp --disable-client \
  --disable-freeciv-manual --disable-ruledit --enable-fcdb=all \
  --enable-ai-static=classic,threaded && make install

FROM phusion/baseimage:latest

RUN apt-get update && apt-get install -y libcurl4-openssl-dev libjansson-dev make automake autoconf libbz2-dev liblzma-dev libicu-dev pkg-config zlib1g-dev libsqlite3-dev zip python3 libtool

COPY --from=build /usr/local/bin/freeciv-server /usr/local/bin
COPY --from=build /usr/local/etc/freeciv /usr/local/etc/freeciv

RUN groupadd -g 999 freeciv && \
    useradd -r -u 999 -g freeciv freeciv
USER freeciv

ENTRYPOINT ["/usr/local/bin/freeciv-server"]
`

func (s *Server) findOrBuildImage(ctx context.Context, commit string) (string, error) {
	f := filters.NewArgs(filters.Arg("label", "freeciv_commit="+commit))
	ss, err := s.Client.ImageList(ctx, types.ImageListOptions{Filters: f})
	if err != nil {
		return "", err
	}
	if len(ss) > 0 {
		return ss[0].ID, nil
	}
	r, w := io.Pipe()
	tw := tar.NewWriter(w)
	go func() {
		df := []byte(fmt.Sprintf(Dockerfile, *freecivRepo))
		if err := tw.WriteHeader(&tar.Header{Name: "Dockerfile", Size: int64(len(df))}); err != nil {
			log.Print(err)
		}
		if _, err = tw.Write(df); err != nil {
			log.Print(err)
		}

		tw.Close()
		w.Close()
	}()

	ibo := types.ImageBuildOptions{
		BuildArgs:      map[string]*string{"freeciv_commit": &commit},
		SuppressOutput: true,
		Labels:         map[string]string{"freeciv_commit": commit},
	}
	ibr, err := s.Client.ImageBuild(ctx, r, ibo)
	if err != nil {
		log.Fatal(err)
		return "", err
	}
	defer ibr.Body.Close()
	dec := json.NewDecoder(ibr.Body)
	var js jsonmessage.JSONMessage

	dec.Decode(&js)
	if err != nil {
		return "", err
	}

	if js.Stream == "" || !strings.HasPrefix(js.Stream, "sha256:") {
		return "", fmt.Errorf("failed to get image id from: %v", js)
	}
	return strings.TrimSpace(strings.TrimPrefix(js.Stream, "sha256:")), nil
}

func (s *Server) Spawn(ctx context.Context, req *pb.SpawnRequest) (*pb.SpawnResponse, error) {
	im, err := s.findOrBuildImage(ctx, req.FreecivVersion)
	if err != nil {
		return nil, err
	}

	c, err := s.Client.ContainerCreate(ctx, &container.Config{
		Image: im,
	}, &container.HostConfig{}, ibo)
	return nil, nil
}
