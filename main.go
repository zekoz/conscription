package main

import (
	"crypto/tls"
	"crypto/x509"
	"flag"
	"fmt"
	"log"
	"net"

	"github.com/docker/docker/client"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	pb "github.com/zekoz/conscription/pkg/conscription"
	"github.com/zekoz/conscription/pkg/server"
)

var (
	port    = flag.Int("port", 10000, "The server port")
	certDir = flag.String("cert_dir", "", "The directory containing cert and key.")
)

func main() {
	flag.Parse()
	lis, err := net.Listen("tcp", fmt.Sprintf("localhost:%d", *port))
	if err != nil {
		log.Fatal(err)
	}

	c, err := client.NewClientWithOpts(client.WithVersion("1.37"))
	if err != nil {
		log.Fatal(err)
	}
	s := server.Server{Client: c}

	cert, err := tls.LoadX509KeyPair(*certDir+"/cert.pem", *certDir+"/key.pem")
	if err != nil {
		log.Fatal(err)
	}

	pool := x509.NewCertPool()
	pool.AddCert(cert.Leaf)

	creds := credentials.NewTLS(&tls.Config{
		Certificates: []tls.Certificate{cert},
		ClientAuth:   tls.RequireAndVerifyClientCert,
		ClientCAs:    pool,
	})

	gs := grpc.NewServer(grpc.Creds(creds))
	pb.RegisterConscriptionServer(gs, &s)
	gs.Serve(lis)
}
